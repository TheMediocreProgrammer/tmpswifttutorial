
/*********************************
Closures
No special keyword.
Similar to blocks in C and lambda's C++
Functions are instances of named closures.
Closures automatically capture values from the surrounding/enclosing context.
*********************************/

/****
Simple Closure

{ (parameters) -> returnType in
statements
}

****/

var greaterThan = { (x: Int, y: Int) -> Bool in
    return x > y
}


/****
Equivalence between functions and closures.
****/

var names = ["Chris", "Alex", "Ewa", "Barry", "Daniella"]

func backwards(s1: String, s2: String) -> Bool {
return s1 > s2
}

// Call to sort using a func
sort(&names, backwards)
names

// Same result with a closure
sort(&names, { (s1: String, s2: String) -> Bool in
    return s1 < s2
    })
names


/****
Inferred parameter and return types
****/

sort(&names, { s1, s2 in return s1 > s2 })
names

/****
Implicit returns
****/
sort(&names, { s1, s2 in s1 < s2 })
names

/****
Shorthand argument names
****/
sort(&names, { $0 > $1 })
names

/****
Trailing closures
****/
sort(&names) { $0 < $1 }
names

var nums = [1,3,5,7,]

var sum = nums.map() {
    $0 + $0 % 2
}

var sum2 = nums.map {
    $0 + $0 % 2
}

/****
Capturing Values (How do you think this works internally?)
Closures are reference types (e.x. based on let)
****/
func makeIncrementor(forIncrement amount: Int) -> (() -> Int) {
    var runningTotal:Int = 0
    return {
        runningTotal += amount
        return runningTotal // TODO: Why can't I drop this line as well?
                            // The compiler gives error: Int is not identical to UInt8
                            // Is that a compiler bug related to type inference or am I missing something here?
    }
}

let incrementByTen = makeIncrementor(forIncrement: 10)

incrementByTen()
incrementByTen()

let incrementBySeven = makeIncrementor(forIncrement: 7)
incrementBySeven()
incrementByTen()

/****
Memory management with closures....TODO here. Add the section in the Memory Management Playground
****/


/*********************************
Extensions

*********************************/

/*********************************
Protocols
*********************************/

/*********************************
Generics
*********************************/

/*********************************
Operator Overloading
*********************************/

/*********************************
*********************************/

/*********************************
*********************************/


