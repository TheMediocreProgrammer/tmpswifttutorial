
for var index = 0; index < 3; ++index {
    println("index is \(index)")
}

for index in 1...5 { // 1..<5
    println("\(index) times 5 is \(index * 5)")
}

let names = ["Anna", "Alex", "Brian", "Jack"]
for name in names {
    println("Hello, \(name)!")
}


let numberOfLegs = ["spider": 8, "ant": 6, "cat": 4]
for (animalName, legCount) in numberOfLegs {
    println("\(animalName)s have \(legCount) legs")
}


// No surprises in while and do-while


var temperatureInFahrenheit = 30
if temperatureInFahrenheit <= 32 {
    println("It's very cold. Consider wearing a scarf.")
} else if temperatureInFahrenheit >= 86 {
    println("It's really warm. Don't forget to wear sunscreen.")
}


let someCharacter: Character = "e"
switch someCharacter {
case "a", "e", "i", "o", "u":
    println("\(someCharacter) is a vowel") // No implicit fallthru
case "b", "c", "d", "f", "g", "h", "j", "k", "l", "m",
"n", "p", "q", "r", "s", "t", "v", "w", "x", "y", "z":
    println("\(someCharacter) is a consonant")
default:
    println("\(someCharacter) is not a vowel or a consonant")
}


// Working with tuples and ranges

let somePoint = (1, 0)
var iii:Int = 29;

switch somePoint {
case (0, 0):
    println("(0, 0) is at the origin")
    iii = 0
case (_, 0): // For such pattern matching to work as expected, it is important to order the cases from specific on top to general going down the page
    println("(\(somePoint.0), 0) is on the x-axis")
    iii = 1
case (0, _):
    println("(0, \(somePoint.1)) is on the y-axis")
    iii = 2
case (-2...2, -2..<2):
    println("(\(somePoint.0), \(somePoint.1)) is inside the box")
    iii = 3
default:
    println("(\(somePoint.0), \(somePoint.1)) is outside of the box")
    iii = 4
}

iii

// some more pattern matching

let yetAnotherPoint = (1, -1)
switch yetAnotherPoint {
case let (x, y) where x == y:
    println("(\(x), \(y)) is on the line x == y")
case let (x, y) where x == -y:
    println("(\(x), \(y)) is on the line x == -y")
case let (x, y):
    println("(\(x), \(y)) is just some arbitrary point")
}


// Labelled break and continue ... Labels are only allowed at the beginning of loop and switch statements
let finalSquare = 25
var board = [Int](count: finalSquare + 1, repeatedValue: 0)
board[03] = +08; board[06] = +11; board[09] = +09; board[10] = +02
board[14] = -10; board[19] = -11; board[22] = -02; board[24] = -08
var square = 0
var diceRoll = 0

gameLoop: while square != finalSquare {
    if ++diceRoll == 7 { diceRoll = 1 }
    switch square + diceRoll {
    case finalSquare:
        // diceRoll will move us to the final square, so the game is over
        break gameLoop
    case let newSquare where newSquare > finalSquare:
        // diceRoll will move us beyond the final square, so roll again
        continue gameLoop
    default:
        // this is a valid move, so find out its effect
        square += diceRoll
        square += board[square]
    }
}
println("Game over!")


