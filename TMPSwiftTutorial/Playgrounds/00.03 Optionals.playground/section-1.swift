
/*********************************
nil
*********************************

nil is a special value. Much more speacial than NULL from C :)
It is so special, that Swift has a lot of syntax revolving around nil - Optionals really.

*/

var pointlessStats = ["A" : 358, "G" : 589, "P" : 94]
pointlessStats["M"]

/*********************************
Optionals
*********************************

Optionals are types of variables about on par with var or let.

Declaration:
var x : Type?

*/

var iOpt: Int?

iOpt = nil
iOpt = 29

var i:Int

// i = nil // ERROR

// i = iOpt // ERROR

/*********************************
Forced Unwrapping
*********************************

Extracting value out of optionals. Also known as Forced Unwrapping
varName!

*/

i = iOpt!

iOpt = nil
i = iOpt! // What does this mean?? ... Run time error


// Safer way to extract optionals
if (iOpt != nil) {
    let j = iOpt!
    // use j
}

// Shortcut
if iOpt != nil {
    let j = iOpt!
}

/*********************************
Optional Binding
*********************************/

if let j = iOpt {
    println("iOpt is useable as \(j)")
    "iOpt is useable as \(j)"
}

"iOpt is useable as \(iOpt)"
i = iOpt!  // Runtime error if iOpt is nil
"iOpt is useable as \(i)"

/*********************************
Implicitly unwrapped optionals
*********************************
var x: Type!
Equivalent to optionals in all respects except the syntax to unwrap the value out of it
var y:Type = x
instead of the y = x!
*/

var iImplicitUnwrap : Int! = nil

iImplicitUnwrap = 27

if let j = iImplicitUnwrap {
    println("iImplicitUnwrap is useable as \(j)")
    "iImplicitUnwrap is useable as \(j)"
}

i = iImplicitUnwrap  // Runtime error if iImplicitUnwrap is nil
"iImplicitUnwrap is useable as \(iImplicitUnwrap)"




