
/*********************************
Classes - Methods

*********************************/

/****
Instance Methods
Unlike functions, the first method param is unnamed by default
The self property is a reference to the instance
The var properties of a const reference can be changed
****/
class Counter {
    var count: Int = 0
    func incrementBy(amount: Int, numberOfTimes: Int) {
        self.count += amount * numberOfTimes
    }
}

let counter = Counter()
counter.incrementBy(5, numberOfTimes: 3)
counter.count = 25

/****
Detour to structures
The mutating keyword
****/
struct CounterStruct {
    var count: Int = 0
    mutating func incrementBy(amount: Int, numberOfTimes: Int) {
        self.count += amount * numberOfTimes
    }
}
let counterStruct = CounterStruct()
// counterStruct.incrementBy(5, numberOfTimes: 3) // ERROR
// counterStruct.count = 25 // ERROR

var counterStructVar = CounterStruct()
counterStructVar.incrementBy(5, numberOfTimes: 3)
counterStructVar.count = 25


/****
Identity versus Equality/Equivalence Operators

=== vs ==
!== vs !=

- “Identical to” means that two constants or variables of class type refer to exactly the same class instance.
- “Equal to” means that two instances are considered “equal” or “equivalent” in value, for some appropriate meaning of “equal”, as defined by the type’s designer.”


****/
let counter2 = counter
counter2 === counter
counter2 !== counter

// counter2 == counter
// counter2 != counter

/*********************************
Classes - Inheritance
No need to inherit from a global base class like NSObject in Objective C
*********************************/
/****
Subclassing
****/
class Vehicle {
    var numberOfWheels = 4
    var maxPassengers = 1
    func description() -> String {
        return "\(numberOfWheels) wheels; up to \(maxPassengers) passengers"
    }
}

// - Subclassing syntax
// - The super keyword
// - initializer
// - Why call super.init() before modifying the inherited properties?
// - No multiple inheritance...we have interfaces for that kind of behavior
class Bicycle: Vehicle {
    init() {
        super.init()
        numberOfWheels = 2
    }
}


/****
Overriding
****/
class Car: Vehicle {
    var speed: Double = 0.0
    init() {
        super.init()
        maxPassengers = 5
        numberOfWheels = 4
    }
    override func description() -> String {
        return super.description() + "; "
            + "traveling at \(speed) mph"
    }
    // Two implications of the override keyword.
    // func description() -> String { // ERROR
    // override func nonExistant () -> String {return "Hello"} // ERROR
}



/*
Overriding properties:
You can override property setters, getters and observers wherever applicable.

So where is it not applicable?
The obvious? places - No setters for an inherited constant or computed property
- No observers for an inherited constant or computed property
- If you already have a setter, you cannot override an observer
*/

/*
Preventing overrides:
Use the @final attribute
Override prevention:
@final var, @final func
Subclassing prevention
@final class
*/


/****
init/deinit
parameter names
modifying const properties
****/
class Celsius {
    let temperatureInCelsius: Double = 0.0
    init(fahrenheit: Double) {
        temperatureInCelsius = (fahrenheit - 32.0) / 1.8
    }
    init(_ fahrenheitInteger: Int) {
        temperatureInCelsius = Double(fahrenheitInteger - 32) / 1.8
    }
    
    func tempModifier(fahrenheit: Double) {
        // temperatureInCelsius = (fahrenheit - 32.0) / 1.8 // ERROR
    }
    deinit {
        // Release any resources that do not get automatically released by object destruction
    }
}

// let boilingPointOfWater = Celsius(212.0) // ERROR
let boilingPointOfWater = Celsius(fahrenheit: 212.0)
let boilingPointOfWaterIsAlso = Celsius(212)

/****
Initializer delegation
Why?
How?
****/

/****
Initializer and inheritance
Show the image with the rules of initializer call order
****/

/****
Deinitializer and inheritance
Inherited and called at the end of subclass deinit regardless of whether or not a subclass has a deinint(meaning there might be an empty deinit added by the compiler in such cases)
****/


