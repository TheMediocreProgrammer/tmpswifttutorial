
/*********************************
Functions 
    func keyword
*********************************/

/****
A simple function with two arguments and returns a value
****/

func halfOpenRangeLength(start: Int, end: Int) -> Int {
    return end - start
}
println(halfOpenRangeLength(1, 10))


/****
A function with zero arguments and no returns value
    Void keyword is not used in the return type, but is implied.
****/

func sayHelloWorld() -> () {
    println("Hello World")
}
sayHelloWorld()


// Shorter syntax. No need to specify the return type if you are not going to return anything
func sayHelloWorldAgain() {
    println("Hello World")
}
sayHelloWorldAgain()

/****
External parameter names
Default values
Overloading
****/

func join(string s1: String = "hello", toString s2: String = "world", withJoiner joiner: String = " ") -> String {
        return s1 + joiner + s2
}

var s1 = join(string: "hello", toString: "world", withJoiner: ", ")
var s2 = join(string: "hello", toString: "world")
// As long as the arguments we provide are in order, we can drop any parameter that has a default value
var s3 = join(toString: "world")
var s4 = join(string: "hello", withJoiner: ", ")

// Overloading without explicitly naming the parameters
func join2(s1: String = "hello", s2: String = "world", joiner: String = " ") -> String {
    return s1 + joiner + s2
}
var s5 = join2(s1: "hello", joiner: ", ")
// var s5 = join2("hello", ", ") // ERROR due to ambiguity

func join3(s1: String = "hello", i2: Int = 2, joiner: String = " ") -> String {
    return s1 + joiner
}
var s6 = join3(s1:"hello", joiner:", ")
// var s6 = join3("hello", ", ") // ERROR despite lack of ambiguity
// var s7 = join3("hello", 3, ",") // ERROR because default arguments have the side effect of using # 

/****
Overloading based on named parameters
Shorthand External Parameter Names
    #
****/

// A func that does forces unnamed parameters
func containsCharacter(string: String, characterToFind: Character) -> Bool {
    for character in string {
        if character == characterToFind {
            return true
        }
    }
    return false
}

containsCharacter("Hello", "H")
//containsCharacter(string: "Hello", characterToFind: "H") // ERROR because the parameters were not intended to be named

// A func that forces named parameters
func containsCharacter(#string: String, #characterToFind: Character) -> Bool {
    for character in string {
        if character == characterToFind {
            return true
        }
    }
    return false
}

containsCharacter(string: "Hello", characterToFind: "H")
// containsCharacter("Hello", "H") // ERROR because the # provides named parameters. And when we have named parameters, we are forced to use them.


/****
Variadic parameters
    Type...
****/

func arithmeticMean(numbers: Double...) -> Double {
    var total: Double = 0
    for number in numbers {
        total += number
    }
    return total / Double(numbers.count)
}
arithmeticMean(1, 2, 3, 4, 5)
arithmeticMean(3, 8, 19)
var primes = [3.0, 5, 7, 11, 13, 17, 19]
arithmeticMean(primes)

/****
inout
    Required only for value type arguments
****/

func swapTwoInts(inout a: Int, inout b: Int) {
    let temporaryA = a
    a = b
    b = temporaryA
}

/****
function types as arguments
****/

// Notice the lack of * that arguments of function point types take.
func printMathResult(mathFunction: (Int, Int) -> Int, a: Int, b: Int) {
    println("Result: \(mathFunction(a, b))")
}
func addTwoInts(a: Int, b: Int) -> Int {
    return a + b
}
func multiplyTwoInts(a: Int, b: Int) -> Int {
    return a * b
}

printMathResult(addTwoInts, 3, 5)
printMathResult(multiplyTwoInts, 3, 5)


/****
Nested functions
Returning functions
Storing context
****/

func chooseStepFunction(backwards: Bool) -> (Int) -> Int {
    func stepForward(input: Int) -> Int { return input + 1 }
    func stepBackward(input: Int) -> Int { return input - 1 }
    return backwards ? stepBackward : stepForward
}
var currentValue = -4
let moveNearerToZero = chooseStepFunction(currentValue > 0)
// moveNearerToZero now refers to the nested stepForward() function
while currentValue != 0 {
    println("\(currentValue)... ")
    currentValue = moveNearerToZero(currentValue)
}
println("zero!")

chooseStepFunction(true)(2)
chooseStepFunction(false)(2)

func chooseStepFunction(backwards: Bool, step: Int) -> (Int) -> Int {
    func stepForward(input: Int) -> Int { return input + step }
    func stepBackward(input: Int) -> Int { return input - step }
    return backwards ? stepBackward : stepForward
}

var stepF = chooseStepFunction(true, 3)(2)
var stepB = chooseStepFunction(false, 5)(2)



