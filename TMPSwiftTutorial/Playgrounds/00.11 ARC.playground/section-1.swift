
/*********************************
ARC - Automatic Reference Counting

Fairly straight-forward. Just need to understand the following concepts:
- Variables referring reference objects result in increment of the reference count of an object. Referring to some other object, decrements the reference count of the previously referred object and increments that for the new object.
- Keywords to understand:
strong
weak
unowned
- Variables, properties and closures capture objects stringly without requiring us to use the strong keyword
- To break reference cycles, use
weak : if the model implies no independent ownership
e.x.,
class Apartment {
weak var tenant: Person?
}
unowned : if the model implies ownership, but no dependency w.r.t. lifetime.
e.x., a credit card needs an owner, but should not dictate the lifetime of the owner
class CreditCard {
unowned let customer:Customer
}

*********************************/

/****
weak references to break retain cycles
****/

var personCount = 0
var apartmentCount = 0
class Person {
    //class var count = 0
    let name: String
    init(_ name: String) {
        self.name = name
        //++Person.count
        ++personCount
    }
    var apartment: Apartment?
    deinit {
        //--Person.count
        --personCount
    }
}

class Apartment {
    //class var count = 0
    let number: Int
    init(_ number: Int) {
        self.number = number
        //++Apartment.count
        ++apartmentCount
    }
    /* weak */ var tenant: Person?
    deinit {
        //--Apartment.count
        --apartmentCount
    }
}

//Person.count
//Apartment.count
personCount
apartmentCount

// Why use optionals here?
var john: Person? = Person("John Appleseed")
var number73: Apartment? = Apartment(73)

//Person.count
//Apartment.count
personCount
apartmentCount

// john?.apartment = number73 // ERROR
john!.apartment = number73 // Could result in runtime error if john is nil
number73!.tenant = john

john = nil
number73 = nil

//Person.count
//Apartment.count
personCount
apartmentCount

// Proof that playground is buggy. (The proof that ARC is not buggy can be obtained by running this program)
var joe:Person? = Person("Joe")
personCount
joe = nil
personCount


/*
Make one of these weak in order to break the retain cycle
weak var apartment: Apartment?
weak var tenant: Person?

Only one of these needs needs to be weak to break the cycle.

*/

/****
unowned references to break the retain cycle
****/
class Customer {
    let name: String
    var card: CreditCard?
    init(name: String) {
        self.name = name
    }
    deinit { println("\(name) is being deinitialized") }
}

class CreditCard {
    let number: Int
    /*unowned */let customer: Customer
    init(number: Int, customer: Customer) {
        self.number = number
        self.customer = customer
    }
    deinit { println("Card #\(number) is being deinitialized") }
}

var jane: Customer? = Customer(name: "Jane Appleseed")
jane!.card = CreditCard(number: 1234_5678_9012_3456, customer: jane!)

jane = nil

/****
So when to use weak versus unowned?
weak: 
    - Logically, when each can have an independant existance. e.g. Person and Apartment objects can exist independantly of each other
    - Syntactically, when references to either are allowed to be nil
unowned:
    - Logically, when the existance of one object does not make sense without the other. e.x., CreditCard without an owning Customer  doesn't make logical sense.
    - Syntactically, when the reference to an object cannot be nil. In such cases we just cannot use weak references because, weak references have to be optionals in order to allow the possibility of getting nil values.
****/



