println("Hello, World!")


/*********************************
ARC - Automatic Reference Counting

Fairly straight-forward. Just need to understand the following concepts:
- Variables referring reference objects result in increment of the reference count of an object. Referring to some other object, decrements the reference count of the previously referred object and increments that for the new object.
- Keywords to understand:
strong
weak
unowned
- Variables, properties and closures capture objects stringly without requiring us to use the strong keyword
- To break reference cycles, use
weak : if the model implies no independent ownership
e.x.,
class Apartment {
weak var tenant: Person?
}
unowned : if the model implies ownership, but no dependency w.r.t. lifetime.
e.x., a credit card needs an owner, but should not dictate the lifetime of the owner
class CreditCard {
unowned let customer:Customer
}

*********************************/

var personCount = 0
var apartmentCount = 0
class Person {
    //class var count = 0
    let name: String
    init(_ name: String) {
        self.name = name
        //++Person.count
        ++personCount
    }
    var apartment: Apartment?
    deinit {
        //--Person.count
        --personCount
    }
}

class Apartment {
    //class var count = 0
    let number: Int
    init(_ number: Int) {
        self.number = number
        //++Apartment.count
        ++apartmentCount
    }
    var tenant: Person?
    deinit {
        //--Apartment.count
        --apartmentCount
    }
}

//Person.count
//Apartment.count
println(personCount)
println(apartmentCount)

// Why use optionals here?
var john: Person? = Person("John Appleseed")
var number73: Apartment? = Apartment(73)

//Person.count
//Apartment.count
println(personCount)
println(apartmentCount)

// john?.apartment = number73 // ERROR
john!.apartment = number73 // Could result in runtime error if john is nil
number73!.tenant = john

john = nil
number73 = nil

//Person.count
//Apartment.count
println(personCount)
println(apartmentCount)

// Proof that ARC or playground is buggy
var joe:Person? = Person("Joe")
println(personCount)
joe = nil
println(personCount)


/*
Make one of these weak in order to break the retain cycle
weak var apartment: Apartment?
weak var tenant: Person?

Only one of these needs needs to be weak to break the cycle.

*/

