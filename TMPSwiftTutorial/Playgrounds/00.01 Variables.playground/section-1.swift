
/*********************************
Variables vs Constants
*********************************/

// Variable definition
var i: Int = 42
i = 50 // Obviously, OK


// Constant definition
let k: Int = 24
// k = 45 // Obviously, ERROR


// Type inference <--- is a really big theme in Swift features
var j = 42 // j is of type Int


// Variables, constants need to be assigned values before they can be used
var w: Int
// w++ // ERROR

/*
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
*/

/*********************************
Value vs Reference Type Variables
*********************************/

// Value types in action
var valI: Int = 100
var valJ = valI

valI
valJ

valJ = 50

valI
valJ

// Reference types in action
class IntHolder {
    var valI : Int = 20;
}

var refI: IntHolder = IntHolder()
var refJ = refI

refI
refJ

refJ.valI = 10

refI
refJ

/*
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
*/

/*********************************
Naming variables/constants
*********************************/

var x = 0.0, y = 0.0, z = 0.0

/*
.
.
.
.
.
.
.
.
.
.
*/

let π = 3.14159
let 你好 = "你好世界"
let 🐶🐮 = "dogcow"



