
/*********************************
Value Types
    (Values are copied when one variable is assigned to the other)
*********************************
- All the basic types like Int, UInt, Float. Double, Boolean, Character, String
- Struct, Enum
- IMPORTANT : All the basic types are actually defined as structures in Swift in order to provide value semantics in their operations: Int, Uint, Double, Float, String



Reference Types:
- Classes, Functions, Closures
- Copying results in both variables/references referring the same instance.
*/

// Integers
Int.min
Int.max
Int8.max
Int16.max
Int32.max
Int64.max

UInt.max
UInt8.max
UInt16.max
UInt32.max
UInt64.max
UInt64.min

// Default type inference
var i = 1_600_000
var i16:Int16 = 0x0F
// i16 = i // ERROR

// Decimals
Float.infinity  // 32 bit (atleast 6 decimal digits)
Double.infinity // 64 bit (atleast 15 decimal digits)

// Default type inference
var deci = 23.1
var f:Float = 1.25e3
// f = deci // ERROR

// Booleans
let niceDayToLeanSwift = true


// Strings
var name: String = "Joh"
var ch: Character = "n"
name += ch // String + Character
name += "ny" // String + String

// String interpolation
let interpolated = "i contains \(i)\n whereas f contains \(f)"
let greeting = "Hello \(name)"

// Helpers
name.uppercaseString
"What Is This".lowercaseString
("Running".lowercaseString).hasSuffix("ing")

// '.' operators Left associativity allows us to get rid of the paranthesis
"Running".uppercaseString.hasPrefix("RUN")

/*
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
*/

/*********************************
Type Aliases
*********************************/

typealias Rank = UInt8

var participantRank: Rank = 10 ; // Reminder that semi-colons are acceptable to the compiler
var kk:UInt8 = 20;
kk = participantRank

/*
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
*/

/*********************************
Collection Types
*********************************/

// Array
// Creation
var arrStrings = ["hello", "world"];
arrStrings.count

// Insertion
arrStrings += ["foo", "bar"]

arrStrings += "baz"

// arrStrings += [25] // ERROR

arrStrings.insert("ocean", atIndex: 3)
arrStrings

// Replacement
arrStrings[2] = "blue"
arrStrings

// Deletion
arrStrings.removeAtIndex(4) // returns the removed entry
arrStrings

arrStrings.removeLast()     // returns the removed entry
arrStrings

// What is possible on constant Arrays?
let kArr = arrStrings

// kArr.removeLast() // ERROR
// kArr[0] = "aloha" // ERROR

// Conclusion: Arrays follow value semantics

// Dictionary
var pointlessStats = ["A" : 358, "G" : 589, "P" : 94]

pointlessStats["A"]

//pointlessStats["G"] = 590.17 // ERROR
pointlessStats["G"] = 590

pointlessStats["M"]
pointlessStats

pointlessStats["M"] = 45
pointlessStats




// About nil ... Let's talk about it in Optionals












