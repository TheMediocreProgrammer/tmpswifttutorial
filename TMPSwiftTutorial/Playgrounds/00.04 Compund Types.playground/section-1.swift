
/*********************************
Structs
*********************************/

struct Resolution {
    var width = 0
    var height = 0
}

let someResolution = Resolution()

let vga = Resolution(width: 640, height: 480)
// let vga2 = Resolution(height: 480, width: 640) // ERROR


/*********************************
Enums
*********************************/
enum CompassPoint {
    case North
    case South
    case East
    case West
}


var directionToHead : CompassPoint = CompassPoint.North

switch directionToHead {
case .North:
    println("Lots of planets have a north")
case .South:
    println("Watch out for penguins")
case .East:
    println("Where the sun rises")
case .West:
    println("Where the skies are blue")
}

// Tuples
var tup : (Int, Float) = (10, 67.0)


enum Barcode {
    case UPCA(Int, Int, Int, Int)
    case QRCode(String)
}

var productBarcode = Barcode.UPCA(8, 85909, 51226, 3)
productBarcode = .QRCode("ABCDEFGHIJKLMNOP")
var pb = Barcode.QRCode("B")

// TODO: switch case and add pattern matching

/****
Getting the raw values
****/

enum CompassPointInt : Int {
    case North = 3
    case South
    case East
    case West
}

let eastNum = CompassPointInt.East.toRaw()

// let eastNum = CompassPoint.East.toRaw() // ERROR


/*********************************
Classes
*********************************/

class VideoMode {
    var resolution = Resolution()
    var interlaced = false
    var frameRate = 0.0
    var name: String?
}

let someVideoMode = VideoMode()


/******
Identity Operations
*******/
let someOtherVideoMode = VideoMode()

if someVideoMode === someOtherVideoMode {
    println("Video modes match")
}

someOtherVideoMode.frameRate = 60.0

if someVideoMode !== someOtherVideoMode {
    println("Video modes do not match")
}


/****
Lazy properties
****/
class DataImporter {
    /*
    DataImporter is a class to import data from an external file.
    The class is assumed to take a non-trivial amount of time to initialize.
    */
    var fileName = "data.txt"
    // the DataImporter class would provide data importing functionality here
}

class DataManager {
    @lazy var importer = DataImporter()
    var data = [String]()
    // the DataManager class would provide data management functionality here
}

let manager = DataManager()
manager.data += "Some data"
manager.data += "Some more data"
// the DataImporter instance for the importer property has not yet been created

println(manager.importer.fileName)
// the DataImporter instance for the importer property has now been created
// prints "data.txt"


/****
Computed properties
****/

struct Point {
    var x = 0.0, y = 0.0
}
struct Size {
    var width = 0.0, height = 0.0
}
struct Rect {
    var origin = Point()
    var size = Size()
    var center: Point {
    get {
        let centerX = origin.x + (size.width / 2)
        let centerY = origin.y + (size.height / 2)
        return Point(x: centerX, y: centerY)
    }
    set(newCenter) {
        origin.x = newCenter.x - (size.width / 2)
        origin.y = newCenter.y - (size.height / 2)
    }
    }
    var bottomRightCorner: Point {
    get {
        let x = origin.x + size.width
        let y = origin.y + size.height
        return Point(x: x, y: y)
    }
    }
    var bottomRightCornerCondensedCode: Point {
        let x = origin.x + size.width
        let y = origin.y + size.height
        return Point(x: x, y: y)
    }
}

/****
Property Observers
****/
class StepCounter {
    var totalSteps: Int = 0 {
    willSet(newTotalSteps) {
        println("About to set totalSteps to \(newTotalSteps)")
    }
    didSet {
        if totalSteps > oldValue  {
            println("Added \(totalSteps - oldValue) steps")
        }
    }
    }
}



/****
Type Properties
****/
struct AudioChannel {
    static let thresholdLevel = 10
    static var maxInputLevelForAllChannels = 0
    var currentLevel: Int = 0 {
    didSet {
        if currentLevel > AudioChannel.thresholdLevel {
            // cap the new audio level to the threshold level
            currentLevel = AudioChannel.thresholdLevel
        }
        if currentLevel > AudioChannel.maxInputLevelForAllChannels {
            // store this as the new overall maximum input level
            AudioChannel.maxInputLevelForAllChannels = currentLevel
        }
    }
    }
}









